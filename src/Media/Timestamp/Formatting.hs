{-# LANGUAGE OverloadedStrings #-}

-- |
--   Module    : Media.Timestamp.Formatting
--   License   : MIT
--   Stability : experimental
--
-- Formatting for `Media.Timestamp`.
module Media.Timestamp.Formatting
  ( timef,
    timecf,
    rangeaf,
  )
where

import Formatting
import Media.Timestamp

-- | Formats a `Time` as `hh:mm:ss.mmm`. This is ffmpeg's format.
--
-- @since 0.1.0.0
timef :: Format r (Time -> r)
timef = accessed hour int % ":" <> accessed minute int % ":" <> accessed second int % "." <> accessed millisecond int

-- | Formats a `Time` as `hh:mm:ss,mmm`. This is .srt format.
--
-- @since 0.1.0.0
timecf :: Format r (Time -> r)
timecf = accessed hour int % ":" <> accessed minute int % ":" <> accessed second int % "," <> accessed millisecond int

-- | Formats a `Range` as "X --> Y", Formatter for SRT format will
-- therefore be rangeaf timecf.
--
-- @since 0.1.1.0
rangeaf :: Format r (Time -> r) -> Format r (Range -> r)
rangeaf x = accessed from x % " --> " <> accessed to x
