# Changelog for simple-media-timestamp-formatting

## v0.1.1.0

* Add `rangeaf` formatter.

## v0.1.0.0

* Add formatters for ffmpeg's timestamp format and srt's timestamp format.
